Source: pynx
Section: science
Priority: optional
Maintainer: Debian PaN Maintainers <debian-pan-maintainers@alioth-lists.debian.net>
Uploaders:
 Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>,
 Picca Frédéric-Emmanuel <picca@debian.org>,
Build-Depends:
 cython3,
 debhelper-compat (= 13),
 dh-sequence-numpy3,
 dh-sequence-python3,
 dh-sequence-sphinxdoc <!nodoc>,
 pandoc <!nodoc>,
 pybuild-plugin-pyproject,
 python3-all-dev,
 python3-fabio (>= 2023.6.0-3~),
 python3-h5py,
 python3-hdf5plugin,
 python3-mako,
 python3-matplotlib,
 python3-nabu,
 python3-nbsphinx <!nodoc>,
 python3-nbsphinx-link <!nodoc>,
 python3-networkx <!nodoc>,
 python3-numexpr,
 python3-numpy,
 python3-nxtomo,
 python3-packaging,
 python3-pooch,
 python3-psutil,
 python3-pydata-sphinx-theme <!nodoc>,
 python3-pyopencl,
 python3-pyvkfft (>= 2024.1.4+ds-3.1~),
 python3-scipy,
 python3-setuptools,
 python3-silx,
 python3-skimage,
 python3-sklearn,
 python3-sphinx <!nodoc>,
 python3-sphinx-argparse <!nodoc>,
 python3-tk <!nodoc>,
 python3-tomoscan,
 sphinx-common,
Standards-Version: 4.1.1
Homepage: https://gitlab.esrf.fr/favre/PyNX
Vcs-Browser: https://salsa.debian.org/science-team/pynx
Vcs-Git: https://salsa.debian.org/science-team/pynx.git

Package: pynx
Architecture: all
Depends:
 python3-pynx (>= ${source:Version}),
 ${misc:Depends},
 ${python3:Depends},
Description: Python tools for Nano-structures Crystallography (Scripts)
 PyNX stands for *Python tools for Nano-structures Crystallography*.
 It is a python library with the following main modules:
 .
 1) pynx.scattering: *X-ray scattering computing using graphical
 processing units*, allowing up to 2.5x10^11 reflections/atoms/seconds
 (single nVidia Titan X). The sub-module``pynx.scattering.gid`` can be
 used for *Grazing Incidence Diffraction* calculations, using the
 Distorted Wave Born Approximation
 .
 2) pynx.ptycho : simulation and analysis of experiments using the
 *ptychography* technique, using either CPU (deprecated) or GPU using
 OpenCL.  Examples are available in the pynx/Examples
 directory. Scripts for analysis of raw data from beamlines are also
 available, as well as using or producing ptychography data sets in
 CXI (Coherent X-ray Imaging) format.
 .
 3) pynx.wavefront: *X-ray wavefront propagation* in the near, far
 field, or continuous (examples available at the end of
 ``wavefront.py``).  Also provided are sub-modules for Fresnel
 propagation and simulation of the illumination from a Fresnel Zone
 Plate, both using OpenCL for high performance computing.
 .
 4) pynx.cdi: *Coherent Diffraction Imaging* reconstruction algorithms
 using GPU.
 .
 In addition, it includes :doc:`scripts <scripts/index>` for
 command-line processing of ptychography data from generic CXI data
 (pynx-ptycho-cxi) or specific to beamlines (pynx-ptycho-id01,
 pynx-ptycho-id13,...).

Package: python3-pynx
Architecture: any
Section: python
Depends:
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
# for bookworm-backports
 python3-fabio (>= 2023.6.0-3~),
 python3-h5py,
 python3-hdf5plugin,
Suggests:
 python-pynx-doc,
Description: Python tools for Nano-structures Crystallography (Python 3)
 PyNX stands for *Python tools for Nano-structures Crystallography*.
 It is a python library with the following main modules:
 .
 1) pynx.scattering: *X-ray scattering computing using graphical
 processing units*, allowing up to 2.5x10^11 reflections/atoms/seconds
 (single nVidia Titan X). The sub-module``pynx.scattering.gid`` can be
 used for *Grazing Incidence Diffraction* calculations, using the
 Distorted Wave Born Approximation
 .
 2) pynx.ptycho : simulation and analysis of experiments using the
 *ptychography* technique, using either CPU (deprecated) or GPU using
 OpenCL.  Examples are available in the pynx/Examples
 directory. Scripts for analysis of raw data from beamlines are also
 available, as well as using or producing ptychography data sets in
 CXI (Coherent X-ray Imaging) format.
 .
 3) pynx.wavefront: *X-ray wavefront propagation* in the near, far
 field, or continuous (examples available at the end of
 ``wavefront.py``).  Also provided are sub-modules for Fresnel
 propagation and simulation of the illumination from a Fresnel Zone
 Plate, both using OpenCL for high performance computing.
 .
 4) pynx.cdi: *Coherent Diffraction Imaging* reconstruction algorithms
 using GPU.
 .
 In addition, it includes :doc:`scripts <scripts/index>` for
 command-line processing of ptychography data from generic CXI data
 (pynx-ptycho-cxi) or specific to beamlines (pynx-ptycho-id01,
 pynx-ptycho-id13,...).
 .
 This package installs the library for Python 3.

Package: python-pynx-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: Python tools for Nano-structures Crystallography (common documentation)
 PyNX stands for *Python tools for Nano-structures Crystallography*.
 It is a python library with the following main modules:
 .
 1) pynx.scattering: *X-ray scattering computing using graphical
 processing units*, allowing up to 2.5x10^11 reflections/atoms/seconds
 (single nVidia Titan X). The sub-module``pynx.scattering.gid`` can be
 used for *Grazing Incidence Diffraction* calculations, using the
 Distorted Wave Born Approximation
 .
 2) pynx.ptycho : simulation and analysis of experiments using the
 *ptychography* technique, using either CPU (deprecated) or GPU using
 OpenCL.  Examples are available in the pynx/Examples
 directory. Scripts for analysis of raw data from beamlines are also
 available, as well as using or producing ptychography data sets in
 CXI (Coherent X-ray Imaging) format.
 .
 3) pynx.wavefront: *X-ray wavefront propagation* in the near, far
 field, or continuous (examples available at the end of
 ``wavefront.py``).  Also provided are sub-modules for Fresnel
 propagation and simulation of the illumination from a Fresnel Zone
 Plate, both using OpenCL for high performance computing.
 .
 4) pynx.cdi: *Coherent Diffraction Imaging* reconstruction algorithms
 using GPU.
 .
 In addition, it includes :doc:`scripts <scripts/index>` for
 command-line processing of ptychography data from generic CXI data
 (pynx-ptycho-cxi) or specific to beamlines (pynx-ptycho-id01,
 pynx-ptycho-id13,...).
 .
 This is the common documentation package.

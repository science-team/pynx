/** Pad an array along the x axis, using a 'reflect_linear' approach (continuous on the border)
* This should be called with d2 as the elementwise array
*
* \param d1: the original array of size (ny, nx)
* \param d2: the new array of size (ny, nx+2*padding)
* \param nx: the width of d1
* \param padding: the padding to be added
*/
__device__ void padx(const int i, float *d2, float *d1, const int nx, const int padding)
{
    const int nx2 = nx + 2*padding;
    const int ix = i % nx2;
    const int iy = i / nx2;
    // Limits of d1 in d2
    const int xleft = padding;
    const int xright = nx+padding;
    // We use a 'reflect_linear' approach
    if(ix<xleft)
    {
        const int dx = xleft-ix-1;
        const float r = 0.5*(float)(dx)/(float)(padding-1);
        d2[i] = (1-r) * d1[             dx + nx * iy]
                +  r  * d1[nx-2*xleft+ dx + nx * iy];
    }
    else if(ix>=xright)
    {
        const int dx = ix-xright;
        const float r = 0.5*(float)dx/(float)(padding-1);
        d2[i] = (1-r) * d1[  nx-dx-1 + nx * iy]
                +  r  * d1[2*xleft-dx-1 + nx * iy];
    }
    else d2[i] = d1[ix - xleft + nx * iy];
}

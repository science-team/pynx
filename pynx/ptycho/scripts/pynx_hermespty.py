#! /opt/local/bin/python

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2016-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr
#   (C) 2020-2024 - Synchrotron SOLEIL
#       authors:
#         Nicolas Mille, nicolas.mille@synchrotron-soleil.fr
#         Picca Frederic-Emmanuel , picca@synchrotron-soleil.fr

import sys
import os
from pynx.ptycho.runner import PtychoRunnerException
from pynx.ptycho.runner.hermes import PtychoRunnerHermes


def main():
    try:
        w = PtychoRunnerHermes(sys.argv, None)
        w.process_scans()
    except PtychoRunnerException as ex:
        print('\n\n Caught exception: %s    \n' % (str(ex)))
        print(f"\nFor the command-line help, use:\n   {os.path.split(sys.argv[0])[-1]} --help ")
        sys.exit(1)
    if "pynx-hermespty.py" in sys.argv[0]:
        print("DEPRECATION warning: please use 'pynx-ptycho-hermes' instead of 'pynx-hermespty.py'")


if __name__ == "__main__":
    main()

#! /opt/local/bin/python
# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2016-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr


import sys
import os
from pynx.ptycho.runner import PtychoRunnerException
from pynx.ptycho.runner.id16a import PtychoRunnerID16a, PtychoRunnerScanID16a


def main():
    try:
        w = PtychoRunnerID16a(sys.argv, None, PtychoRunnerScanID16a)
        w.process_scans()
    except PtychoRunnerException as ex:
        print('\n\n Caught exception: %s    \n' % (str(ex)))
        print(f"\nFor the command-line help, use:\n   {os.path.split(sys.argv[0])[-1]} --help ")
        sys.exit(1)
    if "pynx-id16apty.py" in sys.argv[0]:
        print("DEPRECATION warning: please use 'pynx-ptycho-id16a' instead of 'pynx-id16apty.py'")


if __name__ == '__main__':
    main()

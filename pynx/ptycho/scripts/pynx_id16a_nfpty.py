#! /opt/local/bin/python
# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2016-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr


import sys
import os
from pynx.ptycho.runner import PtychoRunnerException
from pynx.ptycho.runner.id16a_nfp import PtychoRunnerID16aNF, PtychoRunnerScanID16aNF


def main():
    try:
        w = PtychoRunnerID16aNF(sys.argv, None, PtychoRunnerScanID16aNF)
        w.process_scans()
    except PtychoRunnerException as ex:
        print('\n\n Caught exception: %s    \n' % (str(ex)))
        print(f"\nFor the command-line help, use:\n   {os.path.split(sys.argv[0])[-1]} --help ")
        sys.exit(1)
    if "pynx-id16a_nfpty.py" in sys.argv[0]:
        print("DEPRECATION warning: please use 'pynx-ptycho-id16a-nf' instead of 'pynx-id16a_nfpty.py'")


if __name__ == '__main__':
    main()

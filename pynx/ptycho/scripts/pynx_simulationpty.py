#! /opt/local/bin/python
# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2016-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr


import sys
import os
import warnings

from pynx.ptycho.runner import PtychoRunnerException
from pynx.ptycho.runner.simulation import PtychoRunnerSimul, PtychoRunnerScanSimul
from pynx.mpi import MPI


def main():
    try:
        if 'profiling' in sys.argv:
            import cProfile

            r = 0
            if MPI is not None:
                r = MPI.COMM_WORLD.Get_rank()
            cProfile.run('PtychoRunnerSimul(sys.argv, None, PtychoRunnerScanSimul).process_scans()',
                         'profiling-%02d.txt' % r)
        else:
            w = PtychoRunnerSimul(sys.argv, None, PtychoRunnerScanSimul)
            w.process_scans()
    except PtychoRunnerException as ex:
        print('\n\n Caught exception: %s    \n' % (str(ex)))
        print(f"\nFor the command-line help, use:\n   {os.path.split(sys.argv[0])[-1]} --help ")
        sys.exit(1)
    if "pynx-simulationpty.py" in sys.argv[0]:
        print("DEPRECATION warning: please use 'pynx-ptycho-simulation' instead of 'pynx-simulationpty.py'")


if __name__ == '__main__':
    main()

#! /opt/local/bin/python
# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2021-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr

from pynx.version import get_git_version, get_git_date, __version__, __copyright__


def main():
    print("PyNX version: %s" % get_git_version())
    print("Last git commit date: %s" % get_git_date())
    print("Copyright: %s" % __copyright__)
    print()
    has_cuda, has_opencl = False, False
    try:
        from pynx.processing_unit import has_cuda, has_opencl
        print("  cuda support: ", has_cuda)
        print("opencl support: ", has_opencl)
    except:
        print("Error testing for CUDA and OpenCL support")
    print()
    print("Library versions:")
    if has_cuda:
        from pycuda import VERSION_TEXT as v
        print("%12s: %s" % ("pycuda", v))

        try:
            from skcuda.version import __version__ as v
            print("%12s: %s" % ("skcuda", v))
        except ImportError:
            print("%12s: not installed" % "skcuda")

    if has_opencl:
        from pyopencl.version import VERSION_TEXT as v
        print("%12s: %s" % ("pyopencl", v))

    try:
        from pyvkfft.version import __version__ as v
        has_vkfft_cuda, has_vkfft_opencl = False, False
        if has_cuda:
            from pynx.processing_unit.cu_processing_unit import has_vkfft_cuda
        if has_opencl:
            from pynx.processing_unit.cl_processing_unit import has_vkfft_opencl
        print(f"{'pyvkfft':>12}: {v} [FFT support for CUDA:{has_vkfft_cuda}  OpenCL:{has_vkfft_opencl}]")
    except ImportError:
        print("%12s: not installed" % "pyvkfft")

    try:
        from scipy.version import full_version as v
        print("%12s: %s" % ("scipy", v))
    except ImportError:
        print("%12s: not installed" % "scipy")

    try:
        from numpy.version import full_version as v
        print("%12s: %s" % ("numpy", v))
    except ImportError:
        print("%12s: not installed" % "numpy")

    try:
        from matplotlib import __version__ as v
        print("%12s: %s" % ("matplotlib", v))
    except ImportError:
        print("%12s: not installed" % "matplotlib")

    try:
        from h5py import __version__ as v
        print("%12s: %s" % ("h5py", v))
    except ImportError:
        print("%12s: not installed" % "h5py")

    try:
        from hdf5plugin._version import version as v
        print("%12s: %s" % ("hdf5plugin", v))
    except ImportError:
        print("%12s: not installed" % "hdf5plugin")

    try:
        from skimage import __version__ as v
        print("%12s: %s" % ("skimage", v))
    except ImportError:
        print("%12s: not installed ?" % "skimage")

    try:
        from sklearn import __version__ as v
        print("%12s: %s" % ("sklearn", v))
    except ImportError:
        print("%12s: not installed" % "sklearn")
    print("\nTo run pynx tests, use pynx-test")


if __name__ == '__main__':
    main()

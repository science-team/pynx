#! /opt/local/bin/python
# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2023- : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr

import sys
import os
from pynx.cdi.runner import CDIRunnerException
from pynx.cdi.runner.simulation import CDIRunnerSimul, CDIRunnerScanSimul


def main():
    try:
        w = CDIRunnerSimul(sys.argv, None, CDIRunnerScanSimul)
        w.process_scans()
    except CDIRunnerException as ex:
        print('\n\n Caught exception: %s    \n' % (str(ex)))
        print(f"\nFor the command-line help, use:\n   {os.path.split(sys.argv[0])[-1]} --help ")
        sys.exit(1)


if __name__ == '__main__':
    main()

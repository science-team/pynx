#! /opt/local/bin/python
# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2017-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr

import sys
import os
from pynx.cdi.runner import CDIRunnerException
from pynx.cdi.runner.id10 import CDIRunnerID10, CDIRunnerScanID10


def main():
    try:
        w = CDIRunnerID10(sys.argv, None, CDIRunnerScanID10)
        w.process_scans()
    except CDIRunnerException as ex:
        print('\n\n Caught exception: %s    \n' % (str(ex)))
        print(f"\nFor the command-line help, use:\n   {os.path.split(sys.argv[0])[-1]} --help ")
        sys.exit(1)
    if "pynx-id10cdi.py" in sys.argv[0]:
        print("DEPRECATION warning: please use 'pynx-cdi-id10' instead of 'pynx-id10cdi.py'")


if __name__ == '__main__':
    main()

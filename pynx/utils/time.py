from time import perf_counter


class catch_time:
    """
    class to measure elapsed time during an operation.
    For example:
        with catch_time() as timer:
            do_something()
        print('Elapsed time:', timer.dt)
    """
    def __enter__(self):
        self.t0 = perf_counter()
        return self

    def __exit__(self, type, value, traceback):
        self.dt = perf_counter() - self.t0

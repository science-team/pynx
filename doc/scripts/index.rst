.. toctree::
   :maxdepth: 2

.. _scripts:

*****************
Scripts Reference
*****************

Command-line scripts are available, for `Ptychography <ptycho_scripts>`_, `CDI <cdi_scripts>`_
and `holo-tomography <holotom_scripts>`_.

.. _ptycho_scripts:

Ptychography scripts
====================
These are instructions to run the command-line scripts for ptychography analysis
such as `pynx-ptycho-cxi`, `pynx-ptycho-id01`, `pynx-ptycho-id13`, `pynx-ptycho-id16a`, etc...


:doc:`pynx-ptycho-cxi`
    Documentation for the `pynx-ptycho-cxi` command-line script to
    reconstruct ptychography datasets using the CXI file format

:doc:`pynx-ptycho-simulation`
    Documentation for the `pynx-ptycho-simulation` command-line script to
    reconstruct simulated ptychography datasets-mostly for testing & development.


:doc:`pynx-ptycho-id01`
    Documentation for the `pynx-ptycho-id01` command-line script to
    reconstruct ptychography datasets from the ID01 ESRF beamline.


:doc:`pynx-ptycho-id13`
    Documentation for the `pynx-ptycho-id13` command-line script to
    reconstruct ptychography datasets from the ID13 ESRF beamline.

:doc:`pynx-ptycho-id16a`
    Documentation for the `pynx-ptycho-id16a` command-line script to
    reconstruct **far-field** ptychography datasets from the ID16A ESRF beamline.

:doc:`pynx-ptycho-id16a-nf`
    Documentation for the `pynx-ptycho-id16a-nf` command-line script to
    reconstruct **near-field** ptychography datasets from the ID16A ESRF beamline.

:doc:`pynx-ptycho-hermes`
    Documentation for the `pynx-ptycho-hermes` command-line script to
    reconstruct ptychography datasets from the Hermès Soleil beamline.


.. _cdi_scripts:

Coherent Diffraction Imaging scripts
====================================
These are instructions to run the command-line scripts: `pynx-cdi-id01` and `pynx-cdi-id10`.
They can also be used for generic data not from these beamlines.

Most of the parameters are identical, with a some changes in the default parameters
according to each (small-angle or Bragg CDI).

`pynx-cdi-id01`:
----------------

:doc:`pynx-cdi-id01`
    Documentation for the `pynx-cdi-id01` command-line script to
    reconstruct objects from a CDI dataset, with defaults parameters
    for the Bragg CDI case.

`pynx-cdi-id10`:
----------------

:doc:`pynx-cdi-id10`
    Documentation for the `pynx-cdi-id10` command-line script to
    reconstruct objects from a CDI dataset, with defaults parameters
    for the small-angle case (with a beamstop).

CDI-regrid
==========

:doc:`pynx-cdi-regrid`
    Documentation for `pynx-cdi-regrid` to prepare
    3D small-angle cdi CXI file from multiple projections.

Holo-tomography scripts
=======================


`pynx-holotomo-id16b`:
----------------------

:doc:`pynx-holotomo-id16b`
    Documentation for the `pynx-holotomo-id16b` command-line script to
    reconstruct objects from an ID16B holo-tomography dataset.

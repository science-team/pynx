stages:          # List of stages for jobs, and their order of execution
  - build
  - test
  - deploy
  - cleanup

# On the PyNX project, run the CI only for the master & devel branch or for a merge request.
.default_rules:
  rules:
    - if: ($CI_PROJECT_NAME == "PyNX" && ($CI_COMMIT_BRANCH == "master" || $CI_COMMIT_BRANCH == "devel")) || $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_PROJECT_NAME != "PyNX" && $CI_COMMIT_BRANCH != "master"

build-job:       # This job runs in the build stage, which runs first.
  stage: build
  rules:
    - !reference [.default_rules, rules]
  script:
    - echo "Installing pynx..."
    - echo $CI_BUILDS_DIR/pynx/$CI_PIPELINE_ID
    - echo $CI_PROJECT_DIR
    - rm -Rf $CI_BUILDS_DIR/pynx/$CI_PIPELINE_ID
    - mkdir -p $CI_BUILDS_DIR/pynx/$CI_PIPELINE_ID/pynx_env
    - module load mamba cuda
    - which nvcc
    - nvcc --version
    - mamba create --yes --prefix $CI_BUILDS_DIR/pynx/$CI_PIPELINE_ID/pynx_env python=3.12 pip wheel "numpy<2" cython scipy pooch pytest matplotlib fabio ipympl pytools scikit-image silx-base ipython notebook h5py hdf5plugin h5glance ipywidgets psutil scikit-learn mako mpi4py ocl-icd-system numexpr dicttoxml
    - conda activate $CI_BUILDS_DIR/pynx/$CI_PIPELINE_ID/pynx_env
    - pip cache remove pyvkfft
    - pip cache remove pycuda
    - pip cache remove pyopencl
    - pip install pycuda pyvkfft nabu tomoscan pyopencl $CI_PROJECT_DIR

linux_test_api:
  stage: test
  rules:
    - !reference [.default_rules, rules]
  script:
    - echo "Running PyNX API tests... "
    - module load mamba cuda
    - conda activate $CI_BUILDS_DIR/pynx/$CI_PIPELINE_ID/pynx_env
    - which pynx-test
    - pynx-test imports processing_unit cdi holotomo ptycho scattering wavefront

linux_test_cdi_runner:
  stage: test
  rules:
    - !reference [.default_rules, rules]
  script:
    - echo "Running PyNX CDI runner tests... "
    - module load mamba cuda
    - conda activate $CI_BUILDS_DIR/pynx/$CI_PIPELINE_ID/pynx_env
    - which pynx-test
    - pynx-test cdi_runner --oldparse

linux_test_ptycho_runner:
  stage: test
  rules:
    - !reference [.default_rules, rules]
  script:
    - echo "Running PyNX CDI runner tests... "
    - module load mamba cuda
    - conda activate $CI_BUILDS_DIR/pynx/$CI_PIPELINE_ID/pynx_env
    - which pynx-test
    - pynx-test ptycho_runner --oldparse


#lint-test-job:   # This job also runs in the test stage.
#  stage: test    # It can run at the same time as unit-test-job (in parallel).
#  script:
#    - echo "Linting code... This will take about 10 seconds."
#    - pylint --jobs=2 --errors-only $CI_PROJECT_DIR/pynx
#    - echo "No lint issues found."

# Automatically deploy to pynx/devel for a commit in the main branch on the PyNX main repository
deploy-devel-upgrade:
  stage: deploy
  rules:
  - if: ($CI_COMMIT_BRANCH == "devel" || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "devel") && $CI_PROJECT_NAME == "PyNX"
  trigger:
    project: apptainer/conda/pynx
    branch: devel-upgrade
    strategy: depend

# Always remove the environment in the end 
cleanup_job:
  stage: cleanup
  rules:
    - !reference [.default_rules, rules]
  script:
    - rm -Rf $CI_BUILDS_DIR/pynx/$CI_PIPELINE_ID
    - rmdir --ignore-fail-on-non-empty $CI_BUILDS_DIR/pynx
  when: always
